Proyecto 2 Unidad 2 

En este proyecto se busca poder manejar datos en csv 
En nuestro poryecto primero el archivo principal csv se divide toda la informacion que contiene 
en diferentes archivos divididos por el año (entre los años 2008 y 2017) teniendo asi 10 archivos diferentes.

Despues en el codigo principal se crea la ventana principal con los botones de "acerca de " el cual habre otra ventana 
en el cual se puede observar que ola interfaz es del proyecto final de la unidad 2 y el nombre de los autores los cuales son Bastián Morales y Sebastián Rojas. Los otros botones de "editar" el cual habre un ventana donde se pueden editar los datos
seleccionados siendo la fecha, el comentario y el rating, donde los datos editados son agregados al archivo correspondiente, de presionar el 
boton sin aber editado saldra de la ventana de manera normal ya que los datos se mantienen pero si los datos sufren un cambio 
y quedan vacios surgira una ventana de advertencia el cual avisara que deve de agregar nuevos datos si o si y no dejarlos vacios 
por lo tando debe estar seguro de lo que desea editar antes de borrar los datos por default. (lamentablemente no pudimos lograr que la 
edicion de los datos funcionara)
Tambien el boton resumen al presionarlo mostrara un resumen de tipo de droga y tipo de condicion.
El ultimo boton es el de "abrir" el cual abre el filechooser el cual sirve para poder habrir la carpeta contenedora de los diez 
archivos creados con anterioridad (al crear los archivos lo mejor es que todos esten dentro de una carpeta y que esa carpeta
solo contenga estos 10 archivos) al abrir esa carpeta con los archivos el comboboxtext de la ventana principal apareceran 
las opciones con los diferentes años (2008, 2009, ..., 2017) de los cuales dependiendo de cual selecciones, se mostrara en 
el treeview de la ventana principal los datos correspondiende con el año seleccionado. y al seleccionar un fila se mostrara el 
comentario hecho por el pasiente en el label que se encuentra al lado del treeview.
