#importamos pandas y gi 
import pandas as pd
import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk

#creamos la clase para abrir el filechooser
class Dialogo_filechooser():
#funcion para poder controlar el filechooser
    def __init__(self):
        #llamamos los archivos necesarios de glade y filechooser de este mismo 
        builder = Gtk.Builder()
        builder.add_from_file("Proyecto_final.ui")
        self.filechooserR = builder.get_object("filechooser")
        #funciones de los botones primcipales del filechooser
        self.filechooserR.add_buttons(Gtk.STOCK_CANCEL,
                                     Gtk.ResponseType.CANCEL,
                                     Gtk.STOCK_OPEN,
                                     Gtk.ResponseType.OK)
                                    
#mostramos filechooser
        self.filechooserR.show_all()
