#importamos csv y pandas y de os importamos read
import csv
import pandas
from os import read
#cramos una funcion para listar los años 
def lista_year():
    #abrimos el archivo que utilizaremos
    with open("drugsComTest_raw.csv") as archivo_inicial:
        #creamos la variable read para leer el archivo archivo que leimos anteriormente 
        reader = csv.reader(archivo_inicial)
        i = 0
        #creamos las listas 
        anios_existentes = []
        lista_archivo = []
        #recorrimos el archivos con row
        for row in reader:
            #agregamos row en la lista_archivo
            lista_archivo.append(row)
            if i == 0:
                i = 1
                encabezado = row
                continue
            fecha = row[5]
            fecha_lista = fecha.split("-")
            anio = fecha_lista[2]
            #si anios no esta en anios_existentes entonses este se agrega a la lista anios_existente
            if anio not in anios_existentes:
                anios_existentes.append(anio)
        #se retorna reader y las listas creadas
        return reader,anios_existentes,lista_archivo
#se llama a la funcion 
reader,anios_existentes,lista_archivo = lista_year()
#se ordenan los años obtenidos 
anios_existentes.sort()
archivo = pandas.read_csv("drugsComTest_raw.csv")
#recorremos anios_existentes 
for year in anios_existentes:
    #almacema en una variable todo las fechas que tengan el mismo año 
    valor = archivo.loc[archivo["date"].str.contains(f'-{year}')]
    #pasa el dataframe a un archivo csv
    valor.to_csv(f'20{year}.csv')



