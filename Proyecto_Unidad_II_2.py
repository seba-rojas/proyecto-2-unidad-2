#importamos pandas, gi y de pathlib importamos path 
import pandas
import gi
from pathlib import Path
# Selecciona que la versión de GTK a trabajar (3.0)
gi.require_version("Gtk", "3.0")
# Importa Gtk
from gi.repository import Gtk
#de nuestro codigo creado para el filechooser importamos la clase contenida en ese codigo
from Codigo_filechooser import Dialogo_filechooser
#Crearemos una función que devuelve una lista de archivos para posteriormente mostrar la lista
def lista(path):
    return [obj.name for obj in Path(path).iterdir() if obj.is_file()]
#creamos la clase para la ventana principal 
class Ventana_principal():
#creamos la funcion para controlar la vantana
    def __init__(self):
        #llamamos a los archivos necesarios para que funcione bien
        builder = Gtk.Builder()
        builder.add_from_file("Proyecto_final.ui")
        self.ventana = builder.get_object("window")
        self.ventana.set_default_size(800,600)
        self.ventana.set_title("Trabajo de Programación")
        #creamos una lista 
        self.path = []
        #llamamos a los botones y hacemos que llamen a una cierta funcion al hacer click 
        boton_abrir = builder.get_object("btn_abrir")
        boton_abrir.connect("clicked",self.btn_abrir_clicked)

        boton_editar = builder.get_object("btn_editar")
        boton_editar.connect("clicked",self.btn_editar_clicked)

        boton_acerca_de = builder.get_object("btn_acerca_de")
        boton_acerca_de.connect("clicked",self.btn_acerca_de_clicked)

        boton_resumen = builder.get_object("btn_resumen")
        boton_resumen.connect("clicked",self.clicked_resumen)
        #llamos al label para que despues podamos colocarle el comentario de la fila seleccionada 
        self.label_Review_editar = builder.get_object("label_review")
        self.label_Review_editar.set_line_wrap(True)
        #llamamos al comboboxtext 
        self.combo_anios = builder.get_object("combotext_anios")

        #llamamos al treeview
        self.tree = builder.get_object("tree")
        #puede seleccionar la fila a la que le das click
        self.tree.connect("row-activated", self.fila_seleccionada)
        #sirve para cerrar la ventana 
        self.ventana.connect("destroy",Gtk.main_quit)
        self.ventana.show_all()
    #creamos la funcion para abrir el filechooser 
    def btn_abrir_clicked(self,btn=None):
        #llamamos a la clase que fue importada anterirormente
        dialogo = Dialogo_filechooser()
        filechooserS = dialogo.filechooserR
        #realiza la accion para seleccionar y abrir una carpeta 
        filechooserS.set_action(Gtk.FileChooserAction.SELECT_FOLDER)
        filter_csv = Gtk.FileFilter()
        filter_csv.add_pattern("*")
        filter_csv.set_name("DIrectorios")
        filechooserS.add_filter(filter_csv)

        response = filechooserS.run()
        #todo los archivos contenidos en la carpeta seleccionada se guardan las rutas en el combobox de la ventana principal 
        #para poder ser serleccionadas 
        if response == Gtk.ResponseType.OK:
            ruta = filechooserS.get_current_folder()
            self.path = lista(ruta)
            self.combo_anios.append_text("--Seleccione--") 
            for item in self.path:
                self.combo_anios.append_text(item)
            self.combo_anios.set_active(0)
            self.combo_anios.connect("changed", self.crea_vista) 
        elif response == Gtk.ResponseType.CANCEL:
            pass
        filechooserS.destroy()
#funncion para poder seleccionar la fila 
    def fila_seleccionada(self,btn=None, path=None, column=None):

        self.modelo,self.itinerador = self.tree.get_selection().get_selected()
        if self.modelo is None or self.itinerador is None:
            return
        #se crea la varia id el cual se guarda el dato contenido en la primera columna de la fila seleccionada 
        ID = int(self.modelo.get_value(self.itinerador,0))
        self.review = self.data.loc[self.data["uniqueID"]==ID,"review"].iloc[0]
        self.label_Review_editar.set_text(self.review)
#al momento de seleccionar unas de las opciones en el combobox este muestra el archivo seleccionado en el treeview
    def crea_vista(self, cmb = None):
        path_file = self.combo_anios.get_active_text()
        if path_file == "--Seleccione--":
            return
        self.data = pandas.read_csv(path_file)
        datos = self.data.drop(["review"],axis=1)
        datos = datos.drop("Unnamed: 0",axis=1)


        #remueve las columnas que ya estan en el treeview para que no se vayan acumulando al momento de abrir otro archivo 
        if self.tree.get_columns():
            for column in self.tree.get_columns():
                self.tree.remove_column(column)
#crea el modelos de las columnas necesarias en el treeview dependiendo del las columnas del archivo
        largo_columnas = len(datos.columns)
        modelo = Gtk.ListStore(*(largo_columnas * [str]))
        self.tree.set_model(model = modelo)

        cell = Gtk.CellRendererText()
        #agrega los datos a las columnas creadas y las ordena 
        for item in range(len(datos.columns)):
            column = Gtk.TreeViewColumn(datos.columns[item], cell, text=item)
            self.tree.append_column(column)
            column.set_sort_column_id(item)
        #reccore las lineas 
        for item in datos.values:
            line = [str(x) for x in item]
            modelo.append(line)

    #se crea la funcion para el boton que abre la ventana de editar 
    def btn_editar_clicked(self,btn=None):
        #toma los valores de la fila seleccionada 
        self.modelo,self.itinerador = self.tree.get_selection().get_selected()
        if self.modelo is None or self.itinerador is None:
            return
        dialogo_editar = Ventana_editar()
        #y los agrega en los espacios correspondiente y tambien da la opcion de editar 
        numero = int(self.modelo.get_value(self.itinerador,0))
        numero_str = f'Id {numero}'
        dialogo_editar.label_ID_editar.set_text(numero_str)
        dialogo_editar.label_Droga_editar.set_text((self.modelo.get_value(self.itinerador,1)))
        dialogo_editar.label_Condi_editar.set_text((self.modelo.get_value(self.itinerador,2)))

        dialogo_editar.combo_text_raiting.set_active(int(self.modelo.get_value(self.itinerador,3))-1)
        #sacamos el valor de la fecha de la fila seleccionada y las tranformamos en una lista 
        fecha = self.modelo.get_value(self.itinerador,4).split("-")
        fecha_anio = f'20{fecha[2]}'
        #se le asigna a cada mes es una llave al que se le asigna un valor 
        fecha_diccionario = {"Jan":0,"Feb":1,"Mar":2,"Apr":3,"May":4,"Jun":5,"Jul":6,"Aug":7,"Sep":8,"Oct":9,"Nov":10,"Dec":12}
        #se le agrega el dia como numero al gtk.calender 
        dialogo_editar.entry_calender.select_day(int(fecha[0]))
        dialogo_editar.entry_calender.select_month(fecha_diccionario[fecha[1]],int(fecha_anio))
        #se obtiene el textbuffer del textview del comentaio del paciente  
        buffer = dialogo_editar.text_View_comentarios.get_buffer() 
        buffer.set_text(self.review)
        #se comprueba si el comentario esta vacio o fue cambiado 
        response = dialogo_editar.ventana_editable.run()
        if response == Gtk.ResponseType.OK:
            self.valor_commentario = buffer.props.text
            #si en la zona del comentario existe algun dato este se cerrara de manera correcta 
            if self.valor_commentario != "":
                dialogo_editar.ventana_editable.destroy()
            else:
                #de no hacerlo saltara una ventana de advertrencia 
                dialogo_advertencia = Advertencia()
                response = dialogo_advertencia.ventana_dialogo_Advertencia.run()
                if response == Gtk.ResponseType.CLOSE:
                    #la ventana de advertencia de cerrara para que se pueda seguir escribiendo 
                    dialogo_advertencia.ventana_dialogo_Advertencia.destroy()
        dialogo_editar.ventana_editable.destroy()

#funcion para el boton de acerca de 
    def btn_acerca_de_clicked(self,btn=None):
        #llama a la clase acerca de 
        dialogo_acerca = Acerca_de()
        response = dialogo_acerca.ventana_Acerca.run()
#si se preciona el boton cerrar esta se cierra 
        if response == Gtk.ResponseType.CLOSE:
            dialogo_acerca.ventana_Acerca.destroy()
#de apretar el boton dialogo_resumen o creditos se mostraran los creadores de la interfas 
    def clicked_resumen(self,btn = None):
        dialogo_resumen = Ventana_Resumen(self.data)
        response = dialogo_resumen.ventana_Resumen_Text.run()
        if response == Gtk.ResponseType.CLOSE:
            #si se preciona cerrar esta ventana se cierra 
            dialogo_resumen.ventana_Resumen_Text.destroy()
        dialogo_resumen.ventana_Resumen_Text.destroy()
        

#se crea la clase para la ventana editar 
class Ventana_editar():
    def __init__(self):
        #se llaman a los archivos necesarios para el correcto funcionamiento 
        builder = Gtk.Builder()
        builder.add_from_file("Proyecto_final.ui")
        self.ventana_editable = builder.get_object("ventana_dialogo_editar")
        self.ventana_editable.show_all()
        #se llaman a  los label necesarios que se ocuparan despues 
        self.label_ID_editar = builder.get_object("label_ID")
        self.label_Droga_editar = builder.get_object("label_Droga")
        self.label_Condi_editar = builder.get_object("label_Condi")
        #se llama al calender 
        self.entry_calender = builder.get_object("entrada_calender")
        #se llama al textview
        self.text_View_comentarios = builder.get_object("text_view_comentario")
        self.text_View_comentarios.set_wrap_mode(2)
        #se llama al combotext del raiting 
        self.combo_text_raiting = builder.get_object("combobox_raiting")
        #se crea la fila del raiting 
        raiting = []
        #hace que el combotext tenga las opciones a seleccionar 
        for i in range(10):
            numero_string = f'{i+1}'
            raiting.append(numero_string)
        self.combo_text_raiting.set_active(0)
        for posicion in raiting:
            self.combo_text_raiting.append_text(posicion)
            
        #se llaman a los botones de aceptar y de cancelar
        boton_aceptar_edit = builder.get_object("btn_aceptar")
        #boton_aceptar_edit.connect("clicked",self.clicked_aceptar)
        boton_cancelar_edit = builder.get_object("btn_cancelar")

#se crea la clase para la ventana de advertencia
class Advertencia():
    def __init__(self):
        #se llaman a los archivos necesarios 
        builder = Gtk.Builder()
        builder.add_from_file("Proyecto_final.ui")
        self.ventana_dialogo_Advertencia = builder.get_object("ventana_advertencia")
        #se muestra la ventana completa
        self.ventana_dialogo_Advertencia.show_all()

#se crea la clase para la ventana acerca de 
class Acerca_de():
    def __init__(self):
        #se llaman a los archivos necesarios 
        builder = Gtk.Builder()
        builder.add_from_file("Proyecto_final.ui")
        self.ventana_Acerca = builder.get_object("ventana_acerca_de")
        #se muestra la ventana completa 
        self.ventana_Acerca.show_all()
        #se llaman a los botone cerrar y que realice la funcion de cerrar la ventana 
        boton_close = builder.get_object("btn_close")
        boton_close.connect("clicked", self.btn_close_clicked)

    def btn_close_clicked(Self,btn=None):
        pass

#se crea la ventana para el resumen 
class Ventana_Resumen():
    def __init__(self, data):
        #se llaman los archivos necesarios 
        builder = Gtk.Builder()
        builder.add_from_file("Proyecto_final.ui")
        self.ventana_Resumen_Text = builder.get_object("ventana_dialogo_resumen")
        self.ventana_Resumen_Text.set_default_size(800,600)
        #se llaman al label y al boton para cerrar la ventana 
        label_resumen_dialogo = builder.get_object("label_info")
        boton_close_resumen = builder.get_object("btn_close_resumen")
        #se crea una lista que contiene todos los meses

        lista_meses = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
        #se crea una lista para la cantidad de personas por mes 
        cantidad_personas_por_meses = []
        M = 0
        cantidad = 0
        while True:
            #se recorre el archivo revisando la columna de fecha
            for linea in range(len(data)):
                mes = data.at[linea,"date"].split("-")
                #por cada lines se toma el valor mes y se guarda si es igual a la lista meses 
                if mes[1] == lista_meses[M]:
                    #y se va sumando 
                    cantidad += 1
            cantidad_personas_por_meses.append(cantidad)
            M += 1
            cantidad = 0
            if len(cantidad_personas_por_meses) == len(lista_meses):
                break
        
        mayor_rating = 0
        personas_EF = 0
        #se recorre eñ archivo revisando la columna del rating viendo solo los que son 10  
        for linea_2 in range(len(data)):
            if data.at[linea_2,"rating"] == 10:
                mayor_rating += 1
             #se revida la columna de la condicion y se suma solo si este contiene squizofrenia    
            if data.at[linea_2,"condition"] == "Schizophrenia":
                personas_EF +=1
        texto = "\t\t* * * * * *Informacion De Resumen* * * * *\n\n"
        for indice in range(len(lista_meses)):
            #se crean los estring que describen la informcacion que se mostrara
            #se muestra la cantidad de personas que hay por mes  
            texto += f'#  Informacion de personas que fueron el mes de {lista_meses[indice]} : {cantidad_personas_por_meses[indice]} #\n'
            texto += "\n"
            #se muestra la cantidad de veces que hay un rating de 10 
        texto += f'          Informacion de la cantidad de mayor rating: {mayor_rating}\n'
        #se multiplica el asterisco por 40 para separar la informacion 
        texto += "* "*40
        texto += "\n"
        #se muestra la cantidad de personas con squizofrenia 
        texto += f'          Informacion de personas con  Schizophrenia: {personas_EF}\n'

        label_resumen_dialogo.set_label(texto)


if __name__ == "__main__":
    Ventana_principal()
    Gtk.main()
